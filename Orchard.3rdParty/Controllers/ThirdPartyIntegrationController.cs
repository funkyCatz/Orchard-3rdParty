﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Orchard.Caching;
using Orchard.ContentManagement;
using Orchard.Core.Common.Models;
using Orchard.FileSystems.VirtualPath;
using Orchard.MediaLibrary.Fields;
using Orchard.MediaLibrary.Models;
using Orchard.ThirdPartyIntegrationWidget.Constants;
using Orchard.ThirdPartyIntegrationWidget.HtmlProcessor;
using Orchard.ThirdPartyIntegrationWidget.Models;
using Orchard.Widgets.Services;

namespace Orchard.ThirdPartyIntegrationWidget.Controllers {
    public class ThirdPartyIntegrationController : Controller {
        private readonly ICacheManager _cacheManager;
        private readonly IContentManager _contentManager;
        private readonly IWidgetsService _widgetsService;
        private readonly MarkupProcessor _htmlProcessor;
        private readonly ISignals _signals;

        public ThirdPartyIntegrationController(ICacheManager cacheManager, IContentManager contentManager, IWidgetsService widgetsService,
            MarkupProcessor htmlProcessor, ISignals signals, IVirtualPathProvider virtualPathProvider) {
            _cacheManager = cacheManager;
            _contentManager = contentManager;
            _widgetsService = widgetsService;
            _htmlProcessor = new MarkupProcessor(virtualPathProvider);
            _signals = signals;
        }

        protected string Prefix => "ThirdPartyIntegrationWidget";

        [OutputCache(NoStore = true, Duration = 0)]
        public ActionResult GetWidgetContent(string Identity) {
            var htmlString = _cacheManager.Get(Identity, ctx => {
                ctx.Monitor(_signals.When(generateSignal(Identity)));

                var widgetByIdentity = _widgetsService.GetWidgets().FirstOrDefault(x => x.Has<IdentityPart>()
                                             && (x.As<IdentityPart>().Identifier == Identity)).As<ThirdPartyIntegrationPart>();
                if (widgetByIdentity == null) {
                    return null;
                }

                var htmlFileMediaField =
                    widgetByIdentity.Fields.FirstOrDefault(x => String.Equals(x.Name, ThirdPartyIntegrationConstants.HtmlFilePicker)) as
                        MediaLibraryPickerField;

                var htmlMediaPart = _contentManager.Get<MediaPart>(htmlFileMediaField.Ids.FirstOrDefault(), VersionOptions.Published,
                    QueryHints.Empty);

                var jsFileMediaFields =
                    widgetByIdentity.Fields.FirstOrDefault(x => String.Equals(x.Name, ThirdPartyIntegrationConstants.JsFilePicker))
                        as MediaLibraryPickerField;

                IEnumerable<MediaPart> jsMediaParts = null;
                if (jsFileMediaFields != null) {
                    jsMediaParts = _contentManager.GetMany<MediaPart>(jsFileMediaFields.Ids, VersionOptions.Published,
                        QueryHints.Empty);
                }

                _htmlProcessor.TryUpdateHtml(widgetByIdentity, htmlMediaPart, jsMediaParts, out var htmlFileString);

                return htmlFileString;
            });

            if (String.IsNullOrWhiteSpace(htmlString)) {
                return HttpNotFound();
            }

            return Content(htmlString, "text/html");
        }

        private string generateSignal(string identifier) => String.Concat(Prefix, "_", identifier);
        
    }
}
