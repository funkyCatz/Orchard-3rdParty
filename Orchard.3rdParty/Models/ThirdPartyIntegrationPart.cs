﻿using System;
using Orchard.ContentManagement;

namespace Orchard.ThirdPartyIntegrationWidget.Models {
    public class ThirdPartyIntegrationPart : ContentPart {
        public bool IsThirdPartyJsCallsAllowed {
            get { return this.Retrieve(x => x.IsThirdPartyJsCallsAllowed); }
            set { this.Store(x => x.IsThirdPartyJsCallsAllowed, value); }
        }

        public string MarkupFilePath {
            get { return this.Retrieve(x => x.MarkupFilePath); }
            set { this.Store(x => x.MarkupFilePath, value); }
        }

        public DateTime HtmlFileLastWriteTimeUtc {
            get { return this.Retrieve(x => x.HtmlFileLastWriteTimeUtc); }
            set { this.Store(x => x.HtmlFileLastWriteTimeUtc, value); }
        }
    }
}