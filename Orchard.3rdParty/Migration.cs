﻿using Orchard.ContentManagement.MetaData;
using Orchard.Core.Contents.Extensions;
using Orchard.Data.Migration;
using Orchard.ThirdPartyIntegrationWidget.Constants;
using Orchard.ThirdPartyIntegrationWidget.Models;

namespace Orchard.ThirdPartyIntegrationWidget
{
    public class Migration : DataMigrationImpl {
        private readonly string thirdpartyIntegrationWidgetPartName = typeof(ThirdPartyIntegrationPart).Name;

        public int Create() {
            ContentDefinitionManager.AlterPartDefinition(thirdpartyIntegrationWidgetPartName, cfg =>
                cfg
                    .Attachable()
                    .WithField(ThirdPartyIntegrationConstants.HtmlFilePicker, settings =>
                        settings
                            .OfType("MediaLibraryPickerField")
                            .WithDisplayName("HTML File")
                            .WithSetting("MediaLibraryPickerFieldSettings.DisplayedContentTypes", "Document")
                            .WithSetting("MediaLibraryPickerFieldSettings.Required", "true")
                            .WithSetting("MediaLibraryPickerFieldSettings.Hint", "The HTML file that is going to be rendered by a widget."))
                    .WithField(ThirdPartyIntegrationConstants.JsFilePicker, settings =>
                        settings
                            .OfType("MediaLibraryPickerField")
                            .WithDisplayName("JavaScript Files")
                            .WithSetting("MediaLibraryPickerFieldSettings.DisplayedContentTypes", "Document")
                            .WithSetting("MediaLibraryPickerFieldSettings.Multiple", "true")
                            .WithSetting("MediaLibraryPickerFieldSettings.Required", "false")
                            .WithSetting("MediaLibraryPickerFieldSettings.Hint", "The JavaScript files that are going to be linked by a widget.")));

            ContentDefinitionManager.AlterTypeDefinition("ThirdPartyIntegrationWidget", cfg =>
                cfg
                    .WithPart(thirdpartyIntegrationWidgetPartName)
                    .AsWidgetWithIdentity()
                    .DisplayedAs("3rd Party Integration Widget"));

            return 1;
        }
    }
}