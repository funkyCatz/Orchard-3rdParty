﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Orchard.Caching;
using Orchard.ContentManagement.Drivers;
using Orchard.Core.Common.Models;
using Orchard.FileSystems.VirtualPath;
using Orchard.Localization;
using Orchard.Security;
using Orchard.ContentManagement;
using Orchard.MediaLibrary.Fields;
using Orchard.MediaLibrary.Models;
using Orchard.MediaLibrary.Settings;
using Orchard.MediaLibrary.ViewModels;
using Orchard.ThirdPartyIntegrationWidget.Constants;
using Orchard.ThirdPartyIntegrationWidget.HtmlProcessor;
using Orchard.ThirdPartyIntegrationWidget.Models;

namespace ThirdPartyIntegrationWidgets.Drivers {
    public class ThirdPartyIntegrationDriver: ContentPartDriver<ThirdPartyIntegrationPart>
    {
        public Localizer T { get; set; }

        private readonly IAuthenticationService _authenticationService;
        private readonly IVirtualPathProvider _virtualPathProvider;
        private readonly IContentManager _contentManager;
        private readonly ISignals _signals;
        private readonly MarkupProcessor _markupProcessor;

        public ThirdPartyIntegrationDriver(IAuthenticationService authenticationService,
            IVirtualPathProvider virtualPathProvider, ISignals signals, IContentManager contentManager) {
            _authenticationService = authenticationService;
            _virtualPathProvider = virtualPathProvider;
            _contentManager = contentManager;
            _markupProcessor = new MarkupProcessor(virtualPathProvider);
            _signals = signals;
            T = NullLocalizer.Instance;
        }

        protected override string Prefix => "ThirdPartyIntegrationWidget";

        private string generateSignal(string identifier) => String.Concat(Prefix, "_", identifier);

        protected override DriverResult Display(ThirdPartyIntegrationPart part, string displayType, dynamic shapeHelper) {
            if (_authenticationService.GetAuthenticatedUser() == null)
                return null;

            if (part != null) {
                var identityPart = part.Get<IdentityPart>();
                var filePath = _virtualPathProvider.MapPath(part.MarkupFilePath);
                FileInfo fileInfo = new FileInfo(filePath);

                // in case file was changed manually or in Media
                if (fileInfo.LastWriteTime.ToUniversalTime() != part.HtmlFileLastWriteTimeUtc) {
                    _signals.Trigger(generateSignal(identityPart.Identifier));
                }

                return ContentShape("Parts_ThirdPartyIntegrationWidget", () => shapeHelper.Parts_ThirdpartyIntegrationWidget(Model: identityPart.Identifier));
            }

            return ContentShape("Parts_ThirdPartyIntegrationWidget", () => shapeHelper.Parts_ThirdpartyIntegrationWidget(Model: null));
        }

        private String GetPrefixForField(MediaLibraryPickerField field, ContentPart part) {
            return part.PartDefinition.Name + "." + field.Name;
        }

        protected override DriverResult Editor(ThirdPartyIntegrationPart part, dynamic shapeHelper) {
            return ContentShape("Parts_ThirdPartyIntegrationWidget_Edit",
                () => shapeHelper.EditorTemplate(TemplateName: "Parts/ThirdPartyIntegrationWidget", Model: part, Prefix: Prefix));
        }

        protected override DriverResult Editor(ThirdPartyIntegrationPart part, IUpdateModel updater, dynamic shapeHelper) {
            var model = new MediaLibraryPickerFieldViewModel();
            MediaPart htmlMediaPart = null;
            IEnumerable<MediaPart> jsMediaParts = null;

            // Html file validation
            var htmlFileMediaField =
                part.Fields.FirstOrDefault(x => String.Equals(x.Name, ThirdPartyIntegrationConstants.HtmlFilePicker)) as
                    MediaLibraryPickerField;
            if (updater.TryUpdateModel(model, GetPrefixForField(htmlFileMediaField, part), null, null)) {
                var settings =
                    htmlFileMediaField.PartFieldDefinition.Settings.GetModel<MediaLibraryPickerFieldSettings>();

                int htmlMediaPartId = 0;
                if (!String.IsNullOrWhiteSpace(model.SelectedIds)) {
                    htmlMediaPartId = parseIds(model.SelectedIds).FirstOrDefault();
                }

                if (settings.Required && htmlMediaPartId == 0) {
                    updater.AddModelError("Id", T("Please specify a valid HTML file from the Media"));
                    return Editor(part, shapeHelper);
                }

                htmlMediaPart = _contentManager.Get<MediaPart>(htmlMediaPartId, VersionOptions.Published,
                    QueryHints.Empty);
                string filePath = Path.GetExtension(htmlMediaPart.FileName);
                if (!string.Equals(filePath, ".html", StringComparison.InvariantCultureIgnoreCase)
                    && !string.Equals(filePath, ".htm", StringComparison.InvariantCultureIgnoreCase)) {
                    updater.AddModelError("Id",
                        T("HTML file has wrong extension. Current extension: \'{0}\', expected: \'.html\' or \'.htm\' ",
                            filePath));
                    return Editor(part, shapeHelper);
                }
            }

            // Js files validation
            var jsFileMediaFields =
                part.Fields.FirstOrDefault(x => String.Equals(x.Name, ThirdPartyIntegrationConstants.JsFilePicker))
                    as MediaLibraryPickerField;
            if (updater.TryUpdateModel(model, GetPrefixForField(jsFileMediaFields, part), null, null)) {
                int[] jsMediaPartIds = new int[0];
                if (!String.IsNullOrWhiteSpace(model.SelectedIds)) {
                    jsMediaPartIds = parseIds(model.SelectedIds).ToArray();
                }

                jsMediaParts = _contentManager.GetMany<MediaPart>(jsMediaPartIds, VersionOptions.Published,
                    QueryHints.Empty);
                foreach (var mediaPart in jsMediaParts) {
                    string filePath = Path.GetExtension(mediaPart.FileName);
                    if (!string.Equals(filePath, ".js", StringComparison.InvariantCultureIgnoreCase)) {
                        updater.AddModelError("Id",
                            T("JS file \'{0}\' has wrong extension. Current extension: \'{1}\', expected: \'.js\'",
                                mediaPart.FileName, filePath));
                    }
                }
            }

            if (updater.TryUpdateModel(part, Prefix, null, null)) {
                String htmlString;
                if (_markupProcessor.TryUpdateHtml(part, htmlMediaPart, jsMediaParts, out htmlString)) {
                    _signals.Trigger(generateSignal(part.As<IdentityPart>().Identifier));
                }
                else {
                    updater.AddModelError("Id", T("Was not able to parse and update attached HTML file. Please check if attached HTML file is valid."));
                }
            }

            return Editor(part, shapeHelper);
        }

        private IEnumerable<int> parseIds(String ids) {
            return ids.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries)
                    .Select(int.Parse);
        }

    }
}