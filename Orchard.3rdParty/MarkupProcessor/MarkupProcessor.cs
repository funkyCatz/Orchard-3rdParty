﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using HtmlAgilityPack;
using Orchard.FileSystems.VirtualPath;
using Orchard.Logging;
using Orchard.MediaLibrary.Models;
using Orchard.ThirdPartyIntegrationWidget.Models;

namespace Orchard.ThirdPartyIntegrationWidget.HtmlProcessor {
    public class MarkupProcessor {
        private readonly IVirtualPathProvider _virtualPathProvider;

        public MarkupProcessor(IVirtualPathProvider virtualPathProvider) {
            _virtualPathProvider = virtualPathProvider;
            Logger = NullLogger.Instance;
        }

        public ILogger Logger { get; set; }
        public String FilePath { get; set; }

        public bool TryUpdateHtml(ThirdPartyIntegrationPart part, MediaPart htmlFileMediaPart,
            IEnumerable<MediaPart> jsFilesMediaParts, out String htmlString) {
            htmlString = String.Empty;

            if (htmlFileMediaPart == null)
                return false;

            try {
                HtmlDocument document;
                var htmlFilePath = PopulateHtmlDocument(part, htmlFileMediaPart, jsFilesMediaParts, out document);

                //Saving updated document
                var memoryStream = new MemoryStream();
                document.Save(memoryStream);
                memoryStream.Seek(0, SeekOrigin.Begin);
                //Updating timestamp
                FileInfo fileInfo = new FileInfo(htmlFilePath);
                part.HtmlFileLastWriteTimeUtc = fileInfo.LastWriteTime.ToUniversalTime();
                using (StreamReader streamReader = new StreamReader(memoryStream)) {
                    htmlString = streamReader.ReadToEnd();
                }

            }
            catch (Exception ex) {
                Logger.Error(ex, ex.Message);
                return false;
            }
            return true;
        }

        private string PopulateHtmlDocument(ThirdPartyIntegrationPart part, MediaPart htmlFileMediaPart, IEnumerable<MediaPart> jsFilesMediaParts,
            out HtmlDocument document) {
            part.MarkupFilePath = htmlFileMediaPart.MediaUrl;

            String htmlFilePath = _virtualPathProvider.MapPath(part.MarkupFilePath);
            FilePath = Uri.UnescapeDataString(htmlFilePath);

            document = new HtmlDocument();
            document.DetectEncodingAndLoad(FilePath, true);

            var head = document.DocumentNode.Descendants("head").FirstOrDefault();
            if (head == null) {
                head = document.CreateElement("head");
                document.DocumentNode.PrependChild(head);
            }
            // Adding JS files
            var jsfiles = jsFilesMediaParts.Select(p => p.MediaUrl).ToList();
            UpdateJsNodes(document, head, jsfiles);
            //Updating meta information
            UpdateMetaRestrictioNode(document, head, part.IsThirdPartyJsCallsAllowed);
            return htmlFilePath;
        }

        private void UpdateMetaRestrictioNode(HtmlDocument document, HtmlNode head, bool thirdPartyCallsAllowed) {
            var metas = head.SelectNodes("//meta[@http-equiv=\'Content-Security-Policy\']");
            if (metas != null) {
                foreach (var node in metas) {
                    node.Remove();
                }
            }

            if (!thirdPartyCallsAllowed) {
                var meta = document.CreateElement("meta");
                meta.SetAttributeValue("http-equiv", "Content-Security-Policy");
                meta.SetAttributeValue("content", @"default-src 'self'; style-src 'self' 'unsafe-inline';");
                head.PrependChild(meta);
            }
        }

        private void UpdateJsNodes(HtmlDocument document, HtmlNode head, IEnumerable<string> urls) {
            RemoveBetweenComments(head, "Start-ThirdPartyWidgetScripts", "End-ThirdPartyWidgetScripts");

            //Adding scripts
            if (urls.Any()) {
                head.AppendChild(document.CreateComment("<!--Start-ThirdPartyWidgetScripts-->"));
                foreach (var url in urls) {
                    var jsNode = document.CreateElement("script");
                    jsNode.SetAttributeValue("src", url);
                    jsNode.SetAttributeValue("type", "text/javascript");
                    head.AppendChild(jsNode);
                }
                head.AppendChild(document.CreateComment("<!--End-ThirdPartyWidgetScripts-->"));
            }
        }

        private void RemoveBetweenComments(HtmlNode parent, string start, string end) {
            var startNode = parent.SelectSingleNode($"//comment()[contains(., '{start}')]");
            var endNode = parent.SelectSingleNode($"//comment()[contains(., '{end}')]");

            if (startNode != null && endNode != null) {
                int startNodeIndex = startNode.ParentNode.ChildNodes.IndexOf(startNode);
                int endNodeIndex = endNode.ParentNode.ChildNodes.IndexOf(endNode);
                var nodes = startNode.ParentNode.ChildNodes.Where((n, index) => index >= startNodeIndex && index <= endNodeIndex).ToList();
                foreach (var node in nodes) {
                    parent.RemoveChild(node);
                }
            }
        }
    }
}