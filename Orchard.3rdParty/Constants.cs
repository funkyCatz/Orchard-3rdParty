﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Orchard.ThirdPartyIntegrationWidget.Constants
{
    public static class ThirdPartyIntegrationConstants {
        public static readonly string ModuleName = typeof(ThirdPartyIntegrationConstants).Assembly.GetName().Name;
        public const string HtmlFilePicker = "HtmlFilePicker";
        public const string JsFilePicker = "JsFilePicker";
    }
}